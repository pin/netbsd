# rc.elv

# paths
set paths = [
  /bin
  /sbin
  /usr/bin
  /usr/sbin
  /usr/X11R7/bin
  /usr/pkg/bin
  /usr/pkg/sbin
  /usr/games
  /usr/local/bin
]

# environment variables
set-env EDITOR "vim"
set-env CVSEDITOR "vim"
set-env CVSROOT "pin@cvs.NetBSD.org:/cvsroot"
set-env CVS_RSH "ssh"
set-env QT_QPA_PLATFORMTHEME "qt6ct"
set-env USER "pin"
set-env SHELL (which elvish)
set-env TERM alacritty

# prompt
eval (oh-my-posh init elvish --config /usr/pkg/share/oh-my-posh/M365Princess.omp.json)

# alias
fn ls { |@a| lsd -l $@a }
fn lt { |@a| ls --tree $@a }

# startx on login
if (and (eq $E:DISPLAY '') (eq (tty) /dev/constty)) {
  exec startx
}
